package info.androidhive.tabsswipe.adapter;

import java.util.Comparator;

/**
 * Created by click on 30/06/15.
 */
public class Student {

    private int RollNo;
    private String Name;

    public Student(int rollNo, String name) {
        // super();
        this.RollNo = rollNo;
        this.Name = name;
    }

    public Student() {

    }

    public void setName(String name) {
        this.Name = name;

    }

    public void setRollNo(int rollNo) {
        this.RollNo = rollNo;
    }

    public int getRollNo() {
        return RollNo;
    }

    public String getName() {
        return Name;
    }

    public static Comparator<Student> StuNameComparator = new Comparator<Student>() {

        public int compare(Student s1, Student s2) {
            String StudentName1 = s1.getName().toUpperCase();
            String StudentName2 = s2.getName().toUpperCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }
    };
    /*Comparator for sorting the list by roll no*/
    public static Comparator<Student> StuRollno = new Comparator<Student>() {

        public int compare(Student s1, Student s2) {

            int rollno1 = s1.getRollNo();
            int rollno2 = s2.getRollNo();

	   /*For ascending order*/
            return rollno1-rollno2;

	   /*For descending order*/
            //rollno2-rollno1;
        }
    };

    @Override
    public String toString() {
        return "[ rollno=" + RollNo + ", name=" + Name + "]";
    }



}

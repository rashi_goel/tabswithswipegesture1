package info.androidhive.tabsswipe.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.tabsswipe.R;

/**
 * Created by click on 30/06/15.
 */
public class ListAdapter extends BaseAdapter
{

    Context context;
    ArrayList<Student> listStudent;
    LayoutInflater inflater;


    public ListAdapter(Context context, ArrayList<Student> myList) {

        this.context=context;
        this.listStudent=myList;
        this.inflater=LayoutInflater.from(context);

    }


    @Override
    public int getCount() {
       return listStudent.size();
    }


    public Student getItem(int position)
    {
     return listStudent.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

       MyListViewHolder mViewHolder;

        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.row, parent, false);
            mViewHolder = new MyListViewHolder(convertView);
            convertView.setTag(mViewHolder);
        }
        else
        {
            mViewHolder = (MyListViewHolder) convertView.getTag();
        }

        Student myStudent=getItem(position);

        mViewHolder.tvRollNo.setText(String.valueOf(myStudent.getRollNo()));
        mViewHolder.tvName.setText(myStudent.getName());


        return convertView;
    }

    private class MyListViewHolder {

        TextView tvName, tvRollNo;

       public MyListViewHolder(View item) {
            tvRollNo = (TextView) item.findViewById(R.id.txt_roll_no);
            tvName = (TextView) item.findViewById(R.id.txt_name);

        }
    }

    public ArrayList<Student> getListStudent() {
        return listStudent;
    }
}

package info.androidhive.tabsswipe.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by click on 13/07/15.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="studentManager";
    private static final String TABLE_NAME="student";

    //private static final String KEY_ID="_id";
    private static final String KEY_ROLL_NO="Rollno";
    private static final String KEY_NAME="Name";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_STUDENT="CREATE TABLE "+TABLE_NAME+"("+KEY_ROLL_NO+" TEXT PRIMARY KEY, "+KEY_NAME+" TEXT"+")";
        db.execSQL(CREATE_TABLE_STUDENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS"+TABLE_NAME);
        onCreate(db);

    }

    //Adding a new student
    public void insertStudent(Student student)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values= new ContentValues();
        values.put(KEY_ROLL_NO, student.getRollNo());
        values.put(KEY_NAME, student.getName());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }


    //Getting a student
    public Student getStudent(int id)
    {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_NAME, new String[]{ KEY_ROLL_NO, KEY_NAME},KEY_ROLL_NO+"=?",new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor!=null)

            cursor.moveToFirst();

        Student student= new Student(Integer.parseInt(cursor.getString(0)), cursor.getString(1));

        return student;
    }

    // Deleting a single student
    public void deleteStudent(Student student) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ROLL_NO + " = ?",
                new String[] { String.valueOf(student.getRollNo()) });
        db.close();
    }

    public List<Student> getAllStudents()
    {
        List<Student> listStudent= new ArrayList<Student>();
        String selectQuery="Select * from "+TABLE_NAME;
        SQLiteDatabase db= this.getReadableDatabase();
        Cursor cursor=db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst())
        {
            do {
                Student student= new Student();
                student.setRollNo(Integer.parseInt(cursor.getString(0)));
                student.setName(cursor.getString(1));
                listStudent.add(student);
            }
            while (cursor.moveToNext());
        }
        return listStudent;
    }

    public boolean isRollNoExist(int rollNo){

        SQLiteDatabase db= this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " +TABLE_NAME+" WHERE "+KEY_ROLL_NO+ "='" +rollNo+"'" ,null);
        if (c.getCount()>0)
            return true;
        return false;
    }



}

package info.androidhive.tabsswipe.adapter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import info.androidhive.tabsswipe.R;

/**
 * Created by click on 14/07/15.
 */
public class FragmentCreate extends Fragment {



    EditText edtStudentName, edtStudentRollNo;
    Button btnOk, btnClear;

    MainActivity parentActivity;


    public static String TAG="CreateFragment";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        Log.e(TAG, "Inside onCreateView!");
        View view=inflater.inflate(R.layout.create_fragment_layout, container, false);
        edtStudentName= (EditText) view.findViewById(R.id.edt_name);
        edtStudentRollNo= (EditText) view.findViewById(R.id.edt_roll_no);
        btnOk= (Button) view.findViewById(R.id.btn_ok);
        btnClear= (Button) view.findViewById(R.id.btn_clear);

        btnOk.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flag= CheckAllFields();
                if(flag==true)
                {
                    parentActivity= (MainActivity) getActivity();
                    parentActivity.getStudent(Integer.parseInt(edtStudentRollNo.getText().toString()), edtStudentName.getText().toString());
                }
            }
        });

        btnClear.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {


                edtStudentName.setText(null);
                edtStudentRollNo.setText(null);
            }
        });

        return view;
    }

    public boolean CheckAllFields()

    {

        if(!(edtStudentRollNo.getText().toString().length()>0))
        {
            edtStudentRollNo.setError("roll no is Required!");
            return false;

        }
        else if(!(edtStudentName.length()>0))
        {
            edtStudentName.setError(" name is Required!");
            return false;
        }
        else
        {
            return true;
        }
    }

    public void refreshFields()
    {
        System.out.println("Inside refreshFields");
        edtStudentName.setText(null);
        edtStudentRollNo.setText(null);
    }

   /* @Override
    public void onResume() {
        super.onResume();
        edtStudentName.setText(null);
        edtStudentRollNo.setText(null);
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        edtStudentName.setText(null);
        edtStudentRollNo.setText(null);
    }
}

package info.androidhive.tabsswipe.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	Fragment [] fragments;
	Context context;
	private String[] tabs = { "VIEW", "CREATE"};

	public TabsPagerAdapter(FragmentManager fm){
		super(fm);
	}
	public TabsPagerAdapter(FragmentManager fm,Context ctx,Fragment [] fragments) {
		super(fm);
		this.fragments=fragments;
		context=ctx;
	}

	@Override
	public Fragment getItem(int index) {

		return fragments[index];
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return fragments.length;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tabs[position];
	}
}

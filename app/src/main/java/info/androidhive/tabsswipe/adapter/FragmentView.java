package info.androidhive.tabsswipe.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.tabsswipe.R;

/**
 * Created by click on 14/07/15.
 */
public class FragmentView extends Fragment {

    private ListView list;
    ListAdapter customListAdapter;
    MainActivity parentActivity;
    String name;
    int rollno;


    DatabaseHandler db;
    List<Student> listStudent=null;


    public static String TAG="ViewFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view= inflater.inflate(R.layout.view_fragment_layout, container, false);
        list = (ListView) view.findViewById(R.id.list_student_details);

        parentActivity= (MainActivity) getActivity();
//        name=parentActivity.getName();
//        rollno=parentActivity.getRollNo();

        db= new DatabaseHandler(parentActivity);

        if(db==null)
        {
            list.setEmptyView(view);
            customListAdapter= new ListAdapter(parentActivity, new ArrayList<Student>());
        }
        else
        {
            listStudent = db.getAllStudents();
            customListAdapter = new ListAdapter(parentActivity, (ArrayList<Student>) listStudent);
        }

        list.setAdapter(customListAdapter);

        return view;
    }


    public void updateList(int rollno, String name)
    {
        Student s = new Student(rollno, name);
        s.setName(name);
        s.setRollNo(rollno);
        db.insertStudent(s);
        customListAdapter.getListStudent().add(s);
        customListAdapter.notifyDataSetChanged();
    }
}

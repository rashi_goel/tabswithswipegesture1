package info.androidhive.tabsswipe.adapter;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import info.androidhive.tabsswipe.R;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener {

	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	// Tab titles
	private String[] tabs = { "VIEW", "CREATE"};

//	String fetchedName;
//	int fetchedRollNo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Initilization
		Fragment[] fragments = new Fragment[] {new FragmentView(),new FragmentCreate()};

		viewPager= (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager(),this,fragments);

		viewPager.setAdapter(mAdapter);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

	public void getStudent(int rollNo, String name)
	{
		System.out.println("Inside getStudent() in parent Activity");
		if(viewPager.getCurrentItem()==1) //Create Fragment
		{
			System.out.println("viewPager.getCurrentItem()="+viewPager.getCurrentItem());
			FragmentView fragmentView = (FragmentView)getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 0);
			fragmentView.updateList(rollNo,name);
		}
		else  	//View Fragment
		{
			System.out.println("viewPager.getCurrentItem()="+viewPager.getCurrentItem());
			FragmentCreate fragmentCreate= (FragmentCreate) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 1);
			fragmentCreate.edtStudentRollNo.setText(null);
			fragmentCreate.edtStudentName.setText(null);

		}
	}



}
